console.log("Hello World!");

// Arithmetic Operators
console.log(5);
// Addition
console.log(5 + 6);
console.log("5");

// Subtraction
console.log(5 - 6);

// Multiplication
console.log(5 * 6);

// Division
console.log(25 / 5);

console.log(28 / (2 * 7));
console.log(28 / 2 * 7);

// Modulo / Remainder
console.log(2007 % 2);

// Power
console.log(2 ** 3);

// String addition (concatenation)
console.log("Hello" + " " + "World!");
console.log("5" + "5");
console.log(5 + 5);

console.log(5e2);
console.log(5 * (10 ** 2) + 7 - 3); 
console.log(5e-2);
console.log(5 * (10 ** -2));

console.log(-Infinity);
console.log(Infinity);
console.log(NaN);
console.log(Math.round(0.9));
console.log(Math.round(0.4));
console.log(Math.floor(0.9));
console.log(Math.ceil(0.1));

console.log(typeof 5);
console.log(typeof NaN);
console.log(typeof "27.5");

console.log(0.1 + 0.2);
console.log((10 + 20) / 100);



