let user = JSON.parse(sessionStorage.getItem('user'));

if (!user) {
    user = {
        name: prompt('What is your name?'),
        age: parseInt(prompt('How old are you?'))
    };
}

console.log(user);
sessionStorage.setItem('user', JSON.stringify(user));
