

let user = JSON.parse(localStorage.getItem('user'));

if (!user) {
    user = {
        name: prompt('What is your name?'),
        age: parseInt(prompt('How old are you?'))
    };
}

console.log(user);
localStorage.setItem('user', JSON.stringify(user));
