class Animal {
    constructor(name) {
        this.name = name;
    }

    move() {
        console.log('I am moving');
    }
}

class Bird extends Animal {
    constructor(name, wingspan) {
        super(name);
        this.wingspan = wingspan;
    }

    move() {
        console.log('I am flying');
    }
}

class Fish extends Animal {
    constructor(name, fins) {
        super(name);
        this.fins = fins;
    }

    move() {
        console.log('I am swimming');
    }
}

const zoo = [
    new Bird('Eagle', 2),
    new Bird('Sparrow', 1),
    new Fish('Salmon', 2),
    new Fish('Tuna', 3),
];