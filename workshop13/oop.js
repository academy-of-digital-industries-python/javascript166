const product = {
    title: 'Product title',
    price: 100,
    description: 'Product description',
};

class Product {
    constructor(title, price, description) { // method
        if (title  === undefined || price === undefined || description === undefined) {
            throw Error('Missing required properties');
        }

        if (typeof title !== 'string' || typeof price !== 'number' || typeof description !== 'string') {
            throw Error('Incorrect data types');
        }
        
        this.title = title;
        if (price < 0) { 
            throw Error('Price cannot be negative');
        }
        this.price = price;
        this.description = description;
    }

    get_string() {
        return `${this.title} ~ ${this.price} USD`;
    }
};
const products = [
    // {
    //     title: 'Product title 1',
    //     price: 100,
    //     description: 'Product description 1',
    // },
    // {
    //     tite: 'Product title 2',
    //     price: -200,
    //     description: 'Product description 2',
    // },
    // {
    //     title: 'Product title 3',
    //     price: 300,
    //     description: 'Product description 3',
    // }
    new Product('Product title 1', 100, 'Product description 1'),
    new Product('Product title 2', 200, 'Product description 2'),
    new Product('Product title 3', 300, 'Product description 3'),
];

const shampoo = new Product('Shampoo', 10, 'Good shampoo');
const phone = new Product('phone', 5000, 'Good phone');
// console.log(shampoo.title);
// console.log(phone.title, phone.price);
console.log(shampoo.get_string());
console.log(phone.get_string());
for (const product of products) {
    // console.log(product.title);
    // console.log(product.price);
    console.log(product.get_string());
}

