// expiry 10 seconds
document.cookie = 'name=John Doe; expires=' + new Date(Date.now() + 3000).toUTCString();

fetch('https://jsonplaceholder.typicode.com/posts');

async function getPosts() {
    const response = await fetch('https://jsonplaceholder.typicode.com/posts');
    const data = await response.json();
    console.log(data);
}

setTimeout(() => {
    fetch('https://jsonplaceholder.typicode.com/posts');
}, 5000);


getPosts();