const person = {
    firstName: 'Josh',
    lastName: 'Doe',
    age: 45
};

// Change
person.age += 1;
person.age = person.age + 1;

// Access
console.log(person.firstName);
console.log(person.age);
console.log(person['lastName']);

// Add new keys
person.car = 'Mercedes';
person.friends = [
    // ['lika', 27],
    // ['Nika', 25],
    {
        name: 'Lika',
        age: 27
    },
    {
        name: 'Nika',
        age: 25
    }
];

person.friends.pop();
// Deleting keys
delete person.friends;

console.log(person);
// console.log(person.friends[0].name);
// console.log(person.friends[0][0][0]);


