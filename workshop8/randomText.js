// ASCII - American Standard code for in
// 65 - A; 90 - Z
// 97 - a; 122 - z

function randomInt(a, b) {
    return Math.floor((Math.random() * (b - a)) + a);
}

function generateRandomWord(length) {
    let characters = [];
    for (let i = 0; i < length; i++) {
        characters.push(String.fromCharCode(randomInt(97, 122)));
    }

    return characters.join('');
}

function generateRandomText(__wordCount) {
    let text = [];
    let wordCount = 0;

    for (let i = 0; i < __wordCount; i++){
        if (randomInt(0, 100) >= 90 && wordCount > 5) {
            text.push('\n');
            wordCount = 0;
        } else {
            text.push(generateRandomWord(randomInt(5, 19)));
            wordCount++;
        }
    }
    text = text.join(' ');

    text = text.split('\n').map(line => {
        return line.trim();
    })
    return text.join('\n');
}

function checkCorrectness(text) {
    for (let i = 0; i < text.length; i++){
        if (text[i] === '\n') {
            // console.log(JSON.stringify(text.slice(i, i + 2)));
            if (text[i + 1] === ' ') {
                console.log('ERROR');
                break;
            }
        }
    }
}

// console.log(generateRandomWord(15));
// console.log(generateRandomWord(13));
// let text = generateRandomText(10);
// text = text.slice(0, parseInt(text.length / 2)) + ' GAMARJOBA ' + text.slice(parseInt(text.length / 2) + 1)
// console.log(text);
const postsDiv = document.querySelector('#posts');

for (let i = 0; i < 10; i++) {
    const div = document.createElement('div');
    div.classList.add('post');
    div.innerText = generateRandomText(50);  
    postsDiv.appendChild(div);
}