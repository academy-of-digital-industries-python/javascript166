const arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

// arr.forEach(element => console.log(element));

for (const num of arr) { // elements
    console.log(num);
}

for (const i in arr) { // indices
    console.log(i, arr[i]);
}