// Immutable
let firstName = 'Josh';
console.log(firstName.length);
console.log(firstName[1]);
console.log(firstName[firstName.length - 1]);

firstName[0] = 'M'; // Useless
firstName[firstName.length - 1] = 'e'; // You cannot do that
// firstName = 'M' + firstName.slice(1);
// firstName = firstName.slice(0, firstName.length - 1) + 'e';
firstName = firstName.slice(0, firstName.length - 2) + 'l' + firstName.slice(firstName.length - 1);

console.log(firstName);


