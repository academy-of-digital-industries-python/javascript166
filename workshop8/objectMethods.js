const person = {
    firstName: 'Josh',
    lastName: 'Doe',
    age: 45
};


console.log(Object.values(person));
console.log(Object.keys(person));
console.log(Object.entries(person));

console.log('\nValues')
for (const value of Object.values(person)) {
    console.log(value);
}

console.log('\nKeys')
for (const key of Object.keys(person)) {
    console.log(key);
}


console.log('\nEntires')
for (const entry of Object.entries(person)) {
    console.log(entry[0], entry[1]);
}
console.log('\nEntires (Destructure)')
for (const [key, value] of Object.entries(person)) {
    console.log(key, value);
}