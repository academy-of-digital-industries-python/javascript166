function getRandom(a, b) {
    return (Math.random() * (b - a)) + a;
}
function isRoomAvailable(room) {
    return room.available;
}

const hotel = {
    name: 'CareerDevs Mega Hotel',
    rating: Math.round(getRandom(0, 5) * 100) / 100,
    rooms: [
        {
            number: 101,
            available: getRandom(0, 100) >= 50
        },
        {
            number: 102,
            available: getRandom(0, 100) >= 90
        },
        {
            number: 103,
            available: getRandom(0, 100) >= 90
        },
        {
            number: 104,
            available: getRandom(0, 100) >= 90
        },
        {
            number: 105,
            available: getRandom(0, 100) >= 90
        }
    ],
    roomPricing: 200.00
};
const hotelNameElement = document.querySelector('#hotelName');
const hotelRatingElement = document.querySelector('#rating');
const hotelAvailabilityElement = document.querySelector('#availability');
const roomsElement = document.querySelector('#rooms');

hotelNameElement.innerText = hotel.name;
if (hotel.rating < 2.5) {
    hotelRatingElement.classList.add('bg-warning');
} else {
    hotelRatingElement.classList.add('bg-primary');
}
hotelRatingElement.innerText = hotel.rating;
if (hotel.rooms.some(room => room.available)) {
    hotelAvailabilityElement.classList.add('bg-success');
    hotelAvailabilityElement.innerText = 'Yes';
} else {
    hotelAvailabilityElement.innerText = 'No';
    hotelAvailabilityElement.classList.add('bg-danger');
}
// hotelAvailabilityElement.innerText = hotel.rooms.some(room => room.available);
// any === some
const isAvailable = hotel.rooms.some(room => room.available);
console.log(isAvailable); 

// all === every
const allAvailable = hotel.rooms.every(room => room.available);


hotel.rooms
    .filter(isRoomAvailable)
    .forEach(room => {
        const roomDiv = document.createElement('li');
        roomDiv.innerText = room.number;
        roomsElement.appendChild(roomDiv);
    });

// console.log(hotel.rooms.filter(room => room.available));

// Add button primary to html
// const button = document.createElement('button');
// button.classList.add('btn', 'btn-primary');
// button.innerText = 'Book Now';
// document.body.appendChild(button);

console.log(
    hotel.rooms.reduce(
        (prev, room) => prev + room.number,
        0 
    )
);
