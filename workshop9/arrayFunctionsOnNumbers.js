function getRandom(a, b) {
    return Math.floor((Math.random() * (b - a)) + a);
}

function filter(arr, predicate) { // predicate = Math.round
    let newArr = [];
    for (const element of arr) { // 1.8, 7.9, 0.1, 10.9
        if (predicate(element)) { // Math.round(10) -> 0; if (0)
            newArr.push(element);
        }
    }
    return newArr;
}

function map(arr, callback) { // Math.round
    let newArr = [];

    for (const element of arr) { // 1.8, 7.9, 0.1, 10.9
        newArr.push(callback(element)); // Math.round(0.1)
    }
    return newArr;
}

function forEach(arr, callback) {
    for (const element of arr) { // 1.8, 7.9, 0.1, 10.9
        callback(arr); // Math.round(10.9);
    }
}


const numbers = [];

for (let i = 0; i < 100; i++) {
    numbers.push(getRandom(-1000, 1000));
}

console.log(numbers);

console.log('JS Filter', numbers.filter(number => number > 0));
console.log('Our Filter', filter(numbers, (number) => number > 0));


let newArr = [];
for (const number of numbers) {
    if (number > 0) {
        newArr.push(number);
    }
}
console.log(newArr);
console.log(numbers.filter(number => number));
console.log(numbers.filter(number => number < 0));
console.log(numbers.filter(number => number % 2 === 0));
console.log(numbers.filter(number => number === 25));
console.log(
    numbers.filter(number => -150 < number && number < 250)
        .sort((a, b) => a - b)
);


console.log(
    numbers.filter(number => number < 0)
        .map(number => number ** 2)
)

console.log(numbers.filter(Math.round));

const a = [1, 2, 3, 4, 5];

console.log(
    a.map(num => {
        if (num % 2) {
            return num - 1;
        }
        return num;
    })
);


console.log(
    a.reduce((acc, curr) => acc + curr, 0)
);