# JavaScript166

### Assignments

- [How to Submit Assignments](https://www.youtube.com/watch?v=jXpT8eOzzCM)
- [Assignment 1](https://classroom.github.com/a/bjXz7B0Z)
- [Assignment 2](https://classroom.github.com/a/eLBxpzgZ)
- [Assignment 3](https://classroom.github.com/a/mOGXRT3w)
- [Assignment 4](https://classroom.github.com/a/RYKbELw2)
- [Assignment 5](https://classroom.github.com/a/mHe8LieQ)
- [Assignment 6](https://classroom.github.com/a/OGolsoBg)
- [Assignment 7](https://classroom.github.com/a/giqw3qS1)

### Resources

- [JS Book](https://1drv.ms/b/s!AmZJMrBsKhiOh8UDJDRDATZCy9M9VA?e=AYrQHP)
