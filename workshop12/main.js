const btn = document.getElementById("btn");
let clicked = 0;

btn.addEventListener('click', () => {
    if (clicked === 0) {
        btn.style.backgroundColor = "red";
    } else if (clicked === 1) {
        btn.style.backgroundColor = "green";
    } else {
        btn.style.backgroundColor = "blue";
        clicked = 0;
        return;
    }
    clicked++;
})
console.log(btn);