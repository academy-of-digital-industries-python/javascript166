const names = [
    'josh', // 7
    'Luka', // 6
    'Alex', // 1
    'John', // 2
    'Josh', // 3
    'Kyle', // 5
]
const numbers = [
    100,   // 6
    1000,  // 7
    -1000, // 1
    -100,  // 2
    1,     // 4
    -10,   // 3
    5      // 5
];

numbers.sort((a, b) => a - b);
// names.sort();
// console.log(names);
console.log(numbers);
console.log(numbers.concat([5, 8, 9, 100]));
console.log(numbers);

console.log(numbers.indexOf(5));
console.log(numbers.indexOf(-999));
