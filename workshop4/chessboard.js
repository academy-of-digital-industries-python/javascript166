/**
 * Pseudo code
 * შევქმანთ სტრინგი chessboard = ''
 * .#.#.#.#\n
 * row = ' # # # #\n'
 * გაიმეორე ქვედა კოდი მანამ სანამ i < 8 -ზე
 * თუ i არის კენტი დავამატოთ სტრინგში '#' და ჩავსვათ row ში
 * თუ i არის ლუწუა დავამატოთ სტრინგში ' ' და ჩავსვათ row ში
 * i გავზარდოთ 1-ით
 * [ციკლის დასასრული]
 * დაამატე \n
 *
 *
 *
 */

let chessboard = "";
for (let j = 0; j < 8; j++) {
  let row = "";
  for (let i = 0; i < 8; i++) {
    if ((i + j) % 2 === 0) {
      row += "#";
    } else {
      row += " ";
    }
  }
  row += "\n";
  chessboard += row;
}

// console.log(row);
console.log(chessboard);
console.log(JSON.stringify(chessboard));
