function f(x, y) {
    return x + y;
}
function compare(a, b) {
    return a - b;
}

// anonymus / arrow functions
const g = (x, y) => x - y;

const numbers = [-25, 5, 19, 10, -100];
// numbers.sort(compare);
numbers.sort((x, y) => x - y);
console.log(numbers);
