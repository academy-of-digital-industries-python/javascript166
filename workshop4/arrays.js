
const firstName = 'Josh';
let ages = [21, 24, 19, 16, 18, 31, 20]; // values inside an array are called elements
// console.log(firstName[0]);
// console.log(firstName[firstName.length - 1]);

// Accessing element
// index is a number which helps you to access a value
console.log(ages[0]);

console.log(ages[Math.floor(ages.length / 2)]);
console.log(ages[ages.length - 1]);

// Changing elements
// ages[0] = ages[0] + 1;
// ages[0] += 1;
ages[0]++;
console.log(ages);


// Iterate through an array
console.log("\nEven indices");
for (let i = 0; i < ages.length; i += 2) {
    console.log(ages[i]);
}

console.log('\nOdd indices');
for (let i = 1; i < ages.length; i += 2) {
  console.log(ages[i]);
}

// 1 year has passed
console.log(ages);
console.log("\n1 year has passed");
for (let i = 0; i < ages.length; i++) {
    ages[i]++;
}

console.log(ages);


// Adding new elements
console.log("Push");
console.log(ages);
ages.push(61);
console.log(ages);

console.log("Unshift");
console.log(ages);
ages.unshift(33);
console.log(ages);

// Removing elements
console.log("Pop");
console.log(ages);
ages.pop();
// console.log(ages.pop());
console.log(ages);

console.log("Shift");
console.log(ages);
ages.shift();
console.log(ages);
