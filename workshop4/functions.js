// Global Scope
let user = 'hello';
let x = 7;
let t = 12;

function f(x) {
    // x is shadowing x from global scope
    return x ** 2;
}
function power(x, y) {
    return x ** y;
}

// function declaration / definiton
function greetUser(user) {
    let x = 5; // shadowed global scope x
    // Local Scope
    // code block 
    console.log(`Hello ${user}`);
    console.log(t);
    console.log(x);
}

let result = f(5); 
console.log(result);
console.log(f(3));
f(9);
console.log(x);

for (let i = 0; i < 100; i++){
    greetUser("Jenny");
}
console.log(greetUser("Josh"));
console.log(user);

console.log(power(2, 5));