import { productCardDiv } from "./components/product.js";

let allProducts = [];
const productsDiv = document.querySelector('#productsDiv');
const productSearchForm = document.querySelector('#productSearchForm');
const searchInput = document.querySelector('input[name="search"]');

window.addEventListener('load', () => {
    fetch('https://dummyjson.com/products')
        .then(response => response.json())
        .then(data => renderProducts(data.products));
});

const renderProducts = (products) => {
    allProducts = products;
    productsDiv.innerHTML = '';
    products.forEach(
        (product) => {
            productsDiv.appendChild(productCardDiv(product));
        }
    )
};

const searchProducts = (e) => {
    e.preventDefault();
    const url = `https://dummyjson.com/products/search?q=${searchInput.value}`;
    fetch(url)
        .then(response => response.json())
        .then(data => renderProducts(data.products));
}

productSearchForm.addEventListener('submit', searchProducts);
document.querySelector('#sortByPrice').addEventListener('click', (e) => {
    e.preventDefault();
    allProducts = allProducts.sort((a, b) => a.price - b.price);
    renderProducts(allProducts);
})