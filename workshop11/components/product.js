export const productCarouselImage = (image, i) => (`
    <div class="carousel-item ${i === 0 ? 'active' : ''}">
        <img src="${image}" class="d-block w-100" alt="image-${i}" height=300>
    </div>
`);


export const productCardDiv = (product) => {
    const div = document.createElement('div');
    div.classList.add('col');
    console.log(product.images);
    div.innerHTML = (`
    <div class="card mx-auto shadow" style="height: 500px">
        <div id="Carousel-${product.id}" class="carousel slide">
            <div class="carousel-inner">
                ${product.images.map(productCarouselImage)}
            </div>
            <button class="carousel-control-prev bg-dark rounded h-25 my-auto ms-3" 
                type="button" data-bs-target="#Carousel-${product.id}" 
                data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next bg-dark rounded h-25 my-auto me-3" 
                type="button" 
                data-bs-target="#Carousel-${product.id}" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>
        <div class="card-body d-flex flex-column justify-content-between">
            <div class="d-flex justify-content-between align-items-center">
                <h5 class="card-title">
                    ${product.title}
                </h5>
                <div class="d-flex align-items-center">
                    <span>
                        ${product.rating}
                    </span>
                    <span class="material-symbols-outlined ${product.rating > 4.5 ? 'text-warning' : 'text-success'}">
                        star
                    </span>
                </div>
            </div>
            <div class="d-flex justify-content-between">
                <span class="text-success d-flex align-items-center">
                    ${product.price}
                    <span class="material-symbols-outlined">
                        attach_money
                    </span>
                </span>
                <span>
                    <span class="badge text-bg-primary">${product.category}</span>
                    <span class="badge text-bg-success">${product.brand}</span>

                </span>
            </div>
            <p class="card-text">${product.description}</p>
            <a href="#" class="btn btn-primary">Buy</a>
        </div>
    </div>
    `);
    return div;
};