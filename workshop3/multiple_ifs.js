const password = prompt('Enter your password');
const MIN_LENGTH = 7;
const MAX_LENGTH = 15;

if (password.length < MIN_LENGTH) {
    console.log(`password length is less than ${MIN_LENGTH}!`);
}

if (password.length > MAX_LENGTH) {
    console.log(`password length is more than ${MAX_LENGTH}!`);
}

// console.log(password.toLowerCase());
// if (password[0])


// find at least 1 lower case character in string
let passwordHasLowerCharacter = false;
for (let i = 0; i < password.length; i++) {
    const chararacter = password[i];
    if (chararacter.toLowerCase() === chararacter) {
        passwordHasLowerCharacter = true;
        break;
    }
}
if (!passwordHasLowerCharacter) {
    console.log('Password should contain at least 1 lower case character');
}
// string 
