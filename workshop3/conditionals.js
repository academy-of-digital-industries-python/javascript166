let age = 61;

if (age >= 60) {
    for (let i = 0; i < age; i++) {
        console.log("You should not be drinking!");
    }
} else if (age >= 45) {
    console.log("You should be more careful");
} else if (age >= 30) {
    console.log('You should be careful');
} else if (age >= 21) {
    console.log('You are allowed to drink!');
} else {
    console.log('You are not allowed to drink!');
}
