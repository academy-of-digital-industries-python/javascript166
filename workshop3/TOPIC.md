# Topics or Workshop 3

- [While do loops](#while-do-loops)
- [Indefinite Loops](#indefinite-loops)
- [For loops](#for-loops)
- [Breaking out of a loop](#breaking-out-of-a-loop)
- [Skipping the rest of a loop](#skipping-the-rest-of-a-loop)
- [Updating bindings succinctly](#updating-bindings-succinctly)
- [Defining a function](#defining-a-function)
- [Return values](#return-values)
- [The call stack](#the-call-stack)
- [Optional Arguments](#optional-arguments)
- [Scopes](#scopes)
- [Closure](#closure)
- [Recursion](#recursion)
- [Growing functions](#growing-functions)


#### While do loops
while loops are a control structure that allows you to repeat a block of code as long as a condition is true. The loop starts with the keyword while, followed by an expression in parentheses and then a block of code to execute.

```js
let number = 0;
while (number <= 12) {
  console.log(number);
  number = number + 2;
}
```

#### Indefinite Loops
A loop that doesn't have a fixed number of iterations is called an indefinite loop. A while loop is an example of an indefinite loop.

```js
let yourName;
do {
  yourName = prompt("Who are you?");
} while (!yourName);
```

```js
while (true) {
    console.log("This is an indefinite loop");
}
```


#### For loops
A for loop is a control structure that allows you to repeat a block of code a number of times. It starts with the keyword for, followed by an expression in parentheses and then a block of code to execute.

```js
for (let number = 0; number <= 12; number = number + 2) {
  console.log(number);
}
```

#### Breaking out of a loop
The break keyword is used to jump out of a loop and continue executing the code that follows it.

```js
for (let current = 20; ; current = current + 1) {
  if (current % 7 == 0) {
    console.log(current);
    break;
  }
}
```

#### Skipping the rest of a loop
The continue keyword is used to jump out of the current iteration of a loop and continue with the next iteration.

```js
for (let current = 0; current < 10; current = current + 1) {
  if (current % 3 == 0) {
    continue;
  }
  console.log(current);
}
```

#### Updating bindings succinctly
```js
counter = counter + 1;
counter += 1;
```

#### Defining a function
A function is a piece of program wrapped in a value. Such values can be applied in order to run the wrapped program.

```js
let square = function(x) {
  return x * x;
};
console.log(square(12));
```

#### Return values
A function returns a value. If it does not return a value, the value is ```undefined```.

```js
function square(x) {
  console.log(x * x);
}

let x = 3;
console.log(square(x));
```


#### The call stack
The call stack is the mechanism that JavaScript uses to keep track of its place in a script that calls multiple functions.

```js
function greet(who) {
  console.log("Hello " + who);
}
greet("Harry");
greet("Ron");
greet("Hermione");
```

#### Optional Arguments
JavaScript is extremely broad-minded about the number of arguments you pass to a function. If you pass too many, the extra ones are ignored. If you pass too few, the missing parameters get assigned the value ```undefined```.

```js
function minus(a, b) {
  if (b === undefined) return -a;
  else return a - b;
}

console.log(minus(10));
console.log(minus(10, 5));
```

#### Scopes
The collection of bindings and their values that exist at a given time is called the environment. Each block creates a new scope. Parameters and bindings declared in a given scope are local and are not visible from the outside.

```js
let x = 10;
if (true) {
  let y = 20;
  var z = 30;
  console.log(x + y + z);
}
console.log(x + z);
```

```js
// x and y are global bindings
let x = 10;
let y = 20;
function multiply(x, y) {
  // x and y are local bindings
  return x * y;
}
console.log(multiply(x, y));
console.log(multiply(y, x));
console.log(multiply(2, 3));

```


#### Closure
A function that references bindings from local scopes around it is called a closure.

```js
function wrapValue(n) {
  let local = n;
  return function() { return local };
}

let wrap1 = wrapValue(1);
let wrap2 = wrapValue(2);
console.log(wrap1());
console.log(wrap2());
```

#### Recursion
A function that calls itself is called recursive.

```js
function power(base, exponent) {
  if (exponent == 0) {
    return 1;
  } else {
    return base * power(base, exponent - 1);
  }
}

console.log(power(2, 3));
```

