// write a loop that prints numbers from 0 to 100 if number is divisible by 7 and 4
let x = 7;
let y = 4;

for (let i = 0; i < 100; i++) {
    if (i % x !== 0 || i % y !== 0) {
        // skip an iteration
        continue;
    }

    console.log(i);
}