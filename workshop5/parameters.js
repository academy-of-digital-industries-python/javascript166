function f() { // function without any parameters
    console.log('Hi there');
}

function g(x) { // parameter is a variable
    console.log(x ** 2);
}

f();
f();
g(5); // 5 is an argument