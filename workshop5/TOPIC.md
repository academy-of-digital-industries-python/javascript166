# Functions

Function is a block of code that can be called by name. It can accept data and return data as result. It is a way to organize code and make it reusable. 

```js
function name(parameter1, parameter2, parameter3) {
  // code to be executed
  return result;
}
```

## Function Declaration

```js
function myFunction(p1, p2) {
  return p1 * p2;   // The function returns the product of p1 and p2
}
```

## Function Invocation

The code inside the function will execute when "something" invokes (calls) the function.

```js

function myFunction() {
  console.log("Hello World!");
}

myFunction(); // Hello World!
```


## Parameters

Function parameters are the names listed in the function definition.

```js
function myFunction(p1, p2) {
  return p1 * p2;   // The function returns the product of p1 and p2
}
```

## Arguments

Function arguments are the real values passed to the function.

```js
myFunction(4, 3); // 4 and 3 are the arguments
```

## Function Scope

Variables declared inside a function are not accessible from outside the function.

```js
function myFunction() {
  var carName = "Volvo";
  // code here can use carName
}
// code here can't use carName
```

Also the function can access variables declared outside the function.

```js
var carName = "Volvo";
// code here can use carName
console.log(carName);

function myFunction() {
  // code here can use carName
  console.log(carName);
}

console.log(carName);
```

Function parameters are also local to the function.

```js
function myFunction(carName) {
  // code here can use carName
  console.log(carName);
}

// code here can't use carName
```


## Function Return

When JavaScript reaches a return statement, the function will stop executing.

```js
function myFunction(a, b) {
  return a * b;   // Function returns the product of a and b
}
```

## Function Expressions

A JavaScript function can also be defined using an expression.

```js
const f = function (a, b) {return a * b};
```

## Function Hoisting

Function declarations are hoisted to the top of the file.

```js
myFunction(5);

function myFunction(a) {
  console.log(a * a);
}
```

Function expressions are not hoisted.

```js
myFunction(5); // TypeError: myFunction is not a function

var myFunction = function(a) {
  console.log(a * a);
}
```

## Arrow Functions

Arrow functions allow us to write shorter function syntax.

```js
var x = (a, b) => a * b;
```


## Default Parameters

ES6 allows function parameters to have default values.

```js
function myFunction(x, y = 10) {
  // y is 10 if not passed or undefined
  return x + y;
}
```

## Callback and Higher Order Functions

A callback function is a function passed into another function as an argument, which is then invoked inside the outer function to complete some kind of routine or action.

```js
function myDisplayer(some) {
  console.log(some);
}

function myCalculator(num1, num2, myCallback) {
  let sum = num1 + num2;
  myCallback(sum);
}

myCalculator(5, 5, myDisplayer);
```


## Function Closures

A closure is the combination of a function bundled together (enclosed) with references to its surrounding state (the lexical environment). In other words, a closure gives you access to an outer function’s scope from an inner function.

```js
function add(x) {
  return function(y) {
    return x + y;
  };
}

var add5 = add(5);
console.log(add5(2)); // 7
```

## Recursion

Recursion is a technique for iterating over an operation by having a function call itself repeatedly until it arrives at a result.

```js
function factorial(n) {
  if (n === 0) {
    return 1;
  }
  return n * factorial(n - 1);
}

console.log(factorial(5)); // 120
```

## Pure Functions

A pure function is a function where the return value is only determined by its input values, without observable side effects.

```js
function multiply(a, b) {
  return a * b;
}
```

## Impure Functions (Side Effects)

An impure function is a function that mutates the state of the program.

```js
let x = 10;

function impureAdd(y) {
  x = x + y;
}
```

