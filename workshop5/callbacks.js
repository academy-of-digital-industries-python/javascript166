function calc(a, b) {
    return a ** b;
}

function doSomething(a, b, callback, displayer) { // higher order function
    displayer(callback(a, b));
}
function display(result) {
    console.log(result);
}


doSomething(2, 5, calc, display);


