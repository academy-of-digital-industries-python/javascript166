// Anonnymus / arrow function
const f = (a, b) => a + b;

const complexCalc = (a) => a + 100;
const numbers = [1, -56, 78, -199];


console.log(f(5, 7));
// console.log(((a, b) => a + b)(5, 7));
// for (let i = 0; i < numbers.length; i++) {
//     // console.log(complexCalc(numbers[i]));
//     numbers[i] = complexCalc(numbers[i]);
// }

console.log(numbers.map(complexCalc));

console.log(numbers);

