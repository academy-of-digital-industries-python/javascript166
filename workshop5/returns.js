function complexCalculation(a, b) {
    return (a * b) / 7;
}

function withNoReturn() {
    console.log('I have no return');
}
// f(5, 6);

// const f = function (a, b) {
//     console.log(a, b)
// }

// f(5, 6);

let result = complexCalculation(35, 75);
console.log(result);
console.log(withNoReturn());

result += complexCalculation(9, 8);

console.log(result);
