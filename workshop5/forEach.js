const numbers = [1, 2, 3, 4, 5];
const firstNames = ['George', 'john', 'Josh'];

function showAll(arr) {
    for (let i = 0; i < arr.length; i++) {
        console.log(arr[i]);
    }
}
``
function doForEveryElement(arr, someFunction) {
    for (let i = 0; i < arr.length; i++) {
        console.log(someFunction(arr[i]));
    }
}

showAll(numbers);
showAll(firstNames);

// for (let i = 0; i < numbers.length; i++) {
//     console.log(numbers[i]);
// }

// for (let i = 0; i < firstNames.length; i++){
//     console.log(firstNames[i]);
// }

// for (let i = 0; i < numbers.length; i++) {
//     console.log(numbers[i] ** 2);
// }

// for (let i = 0; i < firstNames.length; i++) {
//     console.log(`Hello there ${firstNames[i]}`);
// }
doForEveryElement(numbers, (x) => x ** 2);
doForEveryElement(firstNames, (firstName) => `Hello ${firstName}`);

numbers.forEach((number) => console.log(number ** 2));
numbers.forEach((number) => console.log(number));

