function add(a, b) { // function declaration
    console.log(a + b);
}


add(5, 7); // function call / invocation
add(-5, -12);
add('Hello', ' World');