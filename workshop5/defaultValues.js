function greet(user = 'UNKNOWN') {
    console.log(`Hello ${user}`);
}

greet();
greet('Josh');