const h1 = document.getElementById('header');
// const headers = document.getElementsByClassName('headers');
const headers = Array.from(document.getElementsByTagName('h1'));
const div = document.querySelector('div');

function randomNumber(max) {
    return Math.floor(Math.random() * max);
}

function randomColor() {
    return `rgb(${randomNumber(255)}, ${randomNumber(255)}, ${randomNumber(255)})`
}
headers.forEach(header => {
    header.addEventListener('mouseover', () => {
        header.innerText = 'I have been hovered';
        header.style.backgroundColor = 'red';
    });

    header.addEventListener('click', () =>{
        header.innerText = 'I have been clicked';
        header.style.backgroundColor = 'blue';
    });
})
// console.log(headers);
// h1.addEventListener('mouseover', () => {
//     h1.innerText = 'I have been hovered';
//     h1.style.backgroundColor = 'red';
// });

// h1.addEventListener('click', () =>{
//     h1.innerText = 'I have been clicked';
//     h1.style.backgroundColor = 'blue';

// })

div.addEventListener('click', () => {
    div.innerText = '<p>Hello there</p>';
    div.innerHTML = '<civ>Hello there</p>';
    const anotherDiv = document.createElement('div');
    anotherDiv.innerText = 'I am from js';
    document.body.appendChild(anotherDiv);

    anotherDiv.style.backgroundColor = randomColor();
    anotherDiv.addEventListener('mouseover', () => {
        anotherDiv.style.backgroundColor = randomColor();
    });
    anotherDiv.addEventListener('click', anotherDiv.remove);
    console.log(anotherDiv);
    // div.remove();
})



