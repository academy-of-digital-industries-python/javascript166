// Definition
function greetUser(user) {
    console.log(`Hello ${user}`);
}

function go(direction, distance) {
    console.log(`Going ${direction} ${distance} meters`);
}

function prepareMeal(meal_name = 'pie') {
    const pot = 'Pot 30cm';

    console.log('Gathering Ingredients');
    console.log(`Cook in the oven using ${pot}`);
    return meal_name;
}


function square(x) {
    return x ** 2;
}

// Usage
greetUser('ნიკა');
greetUser('გიო');
greetUser('ჯოში');
greetUser('ჯონი');
greetUser('გიო');
greetUser('მარი');
greetUser();
go('forward', 100);
// goForward(20);
// goForward(40);
go('left', 50);
// console.log(pot);
let myMeal = prepareMeal();
console.log(`Enjoying my ${myMeal}`);
prepareMeal();
console.log(`Eating ${prepareMeal('khinkali')} directly`);

// console.log(`Enjoying my ${myMeal}`);

