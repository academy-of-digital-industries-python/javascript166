function prepareMeal(cookingSteps) { // higher order function
    // cookingSteps is callback function
    console.log('Going to kitchen');
    const meal = cookingSteps();
    console.log(`Brining ${meal} to you!`);
    return meal;
}
function khinkaliSteps() {
    console.log('Beat meat');
    console.log('Prepare dough');
    console.log('boil the khinkali');
    return 'khinkali'; 
}

console.log('day 1')
let meal = prepareMeal(khinkaliSteps);
console.log(`I got delicious ${meal}`);


console.log('day 2')
meal = prepareMeal(khinkaliSteps);
console.log(`I got delicious ${meal}`);
