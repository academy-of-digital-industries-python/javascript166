// Type Casting
let age = Number('25');

// console.log(age + 1);
age++;
console.log(age);

console.log(String(25));
console.log(age.toString());
console.log(parseInt('25.5'));
console.log(parseFloat('25.5'));
console.log(Number('25.5'));
console.log(parseInt(0.1 + 0.2));
