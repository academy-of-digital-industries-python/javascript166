let text = '   Hello    World 123!     ';

console.log(typeof text); // data type of the variable
console.log(text); // value of the variable
console.log(text.length); // number of characters in the string
console.log(text.toLowerCase()); 
console.log(text.toUpperCase());
console.log(text[0]);
console.log(text[1]);
console.log(text[10]);
console.log(text[text.length - 1]);
console.log(text.trim());
console.log('ORIGINAL VALUE', text);

// What is happening here?
console.log(Number('5') + Number(' 5'));
console.log(5 + '5');
console.log('5' + 5);
console.log('5' * 5);
console.log('5' - 5);
console.log(5 * '5');
console.log(5 - '5');
console.log('5' / '5');
console.log('5' / 5);
