let age = 35;
let isAdult = age >= 18;
let isStudent = true;

console.log(typeof isAdult);
console.log(isAdult, isStudent);

// Comparsion operators
console.log('5 > 3', 5 > 3);
// template string
console.log(`5 < 3 = ${5 < 3}`);
console.log(`5 >= 3 = ${5 >= 3}`);
console.log(`5 <= 3 = ${5 <= 3}`);
console.log(`5 == 3 = ${5 == 3}`);
console.log(`5 != 3 = ${5 != 3}`);

// Careful with ==
console.log('Careful with ==')
console.log(`5 == 5 = ${5 == 5}`);
console.log(`5 == '5' = ${5 == '5'}`);
console.log(`5 != '5' = ${5 != '5'}`);

// Strict equality
console.log('Strict equality');
console.log(`5 === 5 = ${5 === 5}`);
console.log(`5 === '5' = ${5 === '5'}`);
console.log(`5 !== '5' = ${5 !== '5'}`);


// Logical operators
let wentToMovies = false;
let atePopcorn = false;
console.log("Logical operators");
console.log(`wentToMovies && atePopcorn = ${wentToMovies && atePopcorn}`);

console.log('Truth table (AND) - &&');
console.log(`true && true = ${true && true}`);
console.log(`true && false = ${true && false}`);
console.log(`flase && true = ${false && true}`);
console.log(`flase && false = ${false && false}`);

console.log('Truth table (OR) - ||');
console.log(`true || true = ${true || true}`);
console.log(`true || false = ${true || false}`);
console.log(`flase || true = ${false || true}`);
console.log(`flase || false = ${false || false}`);

console.log('Truth table (NOT) - !');
console.log(`!true = ${!true}`);
console.log(`!false = ${!false}`);

