let firstName = 'Putin';
let age = 20;

if (age < 18 || firstName === 'Putin') {
    // code block
    console.log('You are not allowed to visit this website');
} else {
    // code block
    console.log(`Welcome to our website ${firstName}`);
}