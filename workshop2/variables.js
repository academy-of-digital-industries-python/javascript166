// declaration of variables
let firstName = 'John'; // hard coded value
let age = 25;
var lastName = 'Doe'; // old way of declaring variables
const PI = 3.14;
//  firstName | camelCase (used in JS)

// first_name | snake_ase (not used in JS)
//  FirstName | PascalCase (used in JS not for variables)
// firstNameAndLast

// firstName = 5;

console.log(firstName, age);
console.log(typeof firstName, typeof age);
console.log(lastName);

// re-assigning variables
firstName = 'Jane';

console.log(firstName);



