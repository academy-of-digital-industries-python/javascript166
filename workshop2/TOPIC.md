## JavaScript Workshop 2 Topics
- [Variables](#variables)
- [Data Types](#data-types)
- [Operators](#operators)
- [If else statement](#if-else-statement)

## Variables
Variables are used to store data. They are declared using the `let` keyword followed by the variable name. Variables can be reassigned and updated. Constants are declared using the `const` keyword and cannot be reassigned.

```javascript
let name = "John";
let age = 25;

console.log(name); // John
console.log(age); // 25

// Reassigning a variable
name = "Jane";
console.log(name); // Jane

// Declaring multiple variables
let x, y, z;
x = 5;
y = 6;
z = x + y;

console.log(z); // 11

// Constants
const PI = 3.14;
console.log(PI); // 3.14
// PI = 3.14159; // This will throw an error
```

## Naming Variables
- Variable names can contain letters, digits, underscores, and dollar signs.
- Variable names cannot start with a digit.
- Variable names are case-sensitive.
- Variable names should be descriptive and meaningful.

```javascript
// Good variable names
let firstName = "John";
let lastName = "Doe";
let age = 25;
let job = "Developer";

// Bad variable names
let first_name = "John";
let last_name = "Doe";
let Age = 25;
let Job = "Developer";

// Error: Variable names cannot start with a digit or contain special characters
// let 1name = "John";
// let #name = "John";
```


## Data Types
JavaScript has dynamic types, which means that the same variable can be used to hold different data types. There are 7 data types in JavaScript but we will focus on the following:
- String
- Number
- Boolean
- Undefined
- Null

Other data types include:
- Object
- Symbol

you can use the `typeof` operator to check the data type of a variable.

```javascript
let name = "John"; // String
let age = 25; // Number
let isMarried = false; // Boolean
let car = null; // Null
let job; // Undefined

console.log(typeof name); // string
console.log(typeof age); // number
console.log(typeof isMarried); // boolean
console.log(typeof car); // object
console.log(typeof job); // undefined
```

## Operators
Operators are used to perform operations on variables and values. JavaScript has the following types of operators:
- Arithmetic Operators
- Assignment Operators
- Comparison Operators
- Logical Operators

Other operators include:
- Bitwise Operators
- Ternary Operator
- String Operators
- Type Operators


```javascript
let x = 5;
let y = 2;

// Arithmetic Operators
console.log(x + y); // 7
console.log(x - y); // 3
console.log(x * y); // 10
console.log(x / y); // 2.5
console.log(x % y); // 1
console.log(x ** y); // 25

// Assignment Operators
let z = 10;
z += 5; // z = z + 5
console.log(z); // 15

// Comparison Operators
console.log(x == y); // false
console.log(x != y); // true
console.log(x > y); // true
console.log(x < y); // false
console.log(x >= y); // true
console.log(x <= y); // false

// Strict Comparison
console.log(x === "5"); // false
console.log(x !== "5"); // true

// Type Conversion
console.log(x == "5"); // true
console.log(x === "5"); // false

// Increment and Decrement Operators
let i = 5;
console.log(i++); // 5
console.log(i); // 6
console.log(++i); // 7
console.log(i); // 7
console.log(i--); // 7
console.log(i); // 6
console.log(--i); // 5
console.log(i); // 5

// Logical Operators
let a = true;
let b = false;
console.log(a && b); // false
console.log(a || b); // true
console.log(!a); // false
```


### If else statement
The `if` statement is used to execute a block of code if a condition is true. The `else` statement is used to execute a block of code if the same condition is false.

```javascript
let age = 25;

if (age >= 18) {
  console.log("You are an adult");
} else {
  console.log("You are a minor");
}
```


## Exercises
1. Declare a variable `name` and assign it your name.
2. Declare a variable `age` and assign it your age.
3. Declare a variable `isAdult` and assign it a boolean value based on your age.
4. Check if the user is an adult and display a message based on the result.



