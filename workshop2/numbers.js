// = assignment operator
let num = 199;

console.log(typeof num); // data type of the variable
console.log(num); // value of the variable
console.log(num + 1); // addition
console.log(num - 1); // subtraction
console.log(num * 2); // multiplication
console.log(num / 2); // division
console.log(num % 2); // modulo
console.log(num ** 2); // exponentiation

console.log('Increase the value of num by 1; starting from =', num);
num = num + 1;
console.log(num);
// add and assign operator
num += 1;
console.log(num);
num++;
console.log(num);

console.log(num++);
console.log(num);
console.log(++num);

console.log(num--);

num /= 2;
num *= 2;
num -= 2;
num %= 2;
num **= 2;

// BigInt

// JavaScript has a limit to the number of digits it can handle.
// The maximum number of digits that JavaScript can handle is 16.
// To handle numbers with more than 16 digits, we can use BigInt.
// BigInt is a new data type in JavaScript that can handle numbers with more than 16 digits.

let num1 = 12736512834172364197635617234618236475324523452345234n;

num1++;
console.log(num1);
